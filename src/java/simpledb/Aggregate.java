package simpledb;

import java.util.*;

/**
 * The Aggregation operator that computes an aggregate (e.g., sum, avg, max,
 * min). Note that we only support aggregates over a single column, grouped by a
 * single column.
 */
public class Aggregate extends Operator {

    private static final long serialVersionUID = 1L;

    private DbIterator child;
    private int aField;
    private int gField;
    private Aggregator.Op op;
    private Aggregator agg;
    private DbIterator it;
    private TupleDesc childSchema;

    /**
     * Constructor.
     *
     * Implementation hint: depending on the type of aField, you will want to
     * construct an {@link IntAggregator} or {@link StringAggregator} to help
     * you with your implementation of readNext().
     *
     *
     * @param child
     *            The DbIterator that is feeding us tuples.
     * @param aField
     *            The column over which we are computing an aggregate.
     * @param gField
     *            The column over which we are grouping the result, or -1 if
     *            there is no grouping
     * @param op
     *            The aggregation operator to use
     * @throws TransactionAbortedException
     * @throws DbException
     */
    public Aggregate(DbIterator child, int aField, int gField, Aggregator.Op op) {
        this.child = child;
        this.aField = aField;
        this.gField = gField;
        this.op = op;

        childSchema = child.getTupleDesc();
        Type aggFieldType = childSchema.getFieldType(aField);
        Type gbFieldType = gField == Aggregator.NO_GROUPING ? null : childSchema.getFieldType(gField);

        switch(aggFieldType) {
        case INT_TYPE:
            agg = new IntegerAggregator(gField, gbFieldType, aField, op);
            break;
        case STRING_TYPE:
            agg = new StringAggregator(gField, gbFieldType, aField, op);
            break;
        }

        try {
            child.open();
            while (child.hasNext()) {
                agg.mergeTupleIntoGroup(child.next());
            }

            it = agg.iterator();
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TransactionAbortedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @return If this aggregate is accompanied by a group by, return the group by
     *         field index in the <b>INPUT</b> tuples. If not, return
     *         {@link simpledb.Aggregator#NO_GROUPING}
     * */
    public int groupField() {
        return gField;
    }

    /**
     * @return If this aggregate is accompanied by a group by, return the name
     *         of the group by field in the <b>OUTPUT</b> tuples If not, return
     *         null;
     * */
    public String groupFieldName() {
        if (gField != Aggregator.NO_GROUPING) {
            return childSchema.getFieldName(gField);
        }

        return null;
    }

    /**
     * @return the aggregate field
     * */
    public int aggregateField() {
        return aField;
    }

    /**
     * @return return the name of the aggregate field in the <b>OUTPUT</b>
     *         tuples
     * */
    public String aggregateFieldName() {
        return childSchema.getFieldName(aField);
    }

    /**
     * @return return the aggregate operator
     * */
    public Aggregator.Op aggregateOp() {
        return op;
    }

    public static String nameOfAggregatorOp(Aggregator.Op op) {
        return op.toString();
    }

    public void open() throws NoSuchElementException, DbException,
	    TransactionAbortedException {
        it.open();
        super.open();
    }

    /**
     * Returns the next tuple. If there is a group by field, then the first
     * field is the field by which we are grouping, and the second field is the
     * result of computing the aggregate, If there is no group by field, then
     * the result tuple should contain one field representing the result of the
     * aggregate. Should return null if there are no more tuples.
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (it.hasNext()) {
            Tuple t = it.next();
            return t;
        }
        return null;
    }

    public void rewind() throws DbException, TransactionAbortedException {
        it.rewind();
    }

    /**
     * Returns the TupleDesc of this Aggregate. If there is no group by field,
     * this will have one field - the aggregate column. If there is a group by
     * field, the first field will be the group by field, and the second will be
     * the aggregate value column.
     *
     * The name of an aggregate column should be informative. For example:
     * "aggName(aop) (child_td.getFieldName(afield))" where aop and afield are
     * given in the constructor, and child_td is the TupleDesc of the child
     * iterator.
     */
    public TupleDesc getTupleDesc() {
    	// TODO: Do what this says
    	return it.getTupleDesc();
    }

    public void close() {
        it.close();
        super.close();
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[] { child };
    }

    @Override
    public void setChildren(DbIterator[] children) {
        // This should throw up instead.
        if (children.length != 1) {
            return;
        }

        child = children[0];
    }

    public String toString() {
        return String.format("<Aggregate agg=%s, op=%s, aField=%d, gField=%d, child=%s>", agg, op, aField, gField, child);
    }

}
