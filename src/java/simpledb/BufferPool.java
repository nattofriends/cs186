package simpledb;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Set;
import java.util.HashSet;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 */
public class BufferPool {
    private static final boolean DEBUG = false;
    /** Bytes per page, including header. */
    public static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;

    /** The main pool structure. */
    private HashMap<PageId, Page> pool;
    private int maxPages;

    /** For LRU use. Recent use goes at the end. */
    private LinkedList<Page> lruPool;

    /** A list of transactions and all the pages they each have locks on. */
    private ConcurrentHashMap<TransactionId, HashSet<PageId>> txns;
    /** A map from each page to a PageLock which represents all the locks that everyone has on that page */
    private ConcurrentHashMap<PageId, PageLock> lox;

    /** The waits-for graph of transactions handled by this BufferPool. */
    private DeadlockGraph deadlockGraph;

    /**
     * There's one of these per page, created the first time a transaction
     * requests a lock.
     */
    private class PageLock {
        // Note: this is not write-biased (does not let waiting writers through first)
        /**
         * You should leave this false because this class generates a LOT of
         * output (liable to crash {JUnit, the JVM, Ant, bash, tmux}).
         */
        private static final boolean DEBUG = false;

        // There are many readers...
        private Set<TransactionId> readers;
        // ...but only one writer
        private TransactionId writer;

        public PageLock() {
            readers = new HashSet<TransactionId>();
            debug("PageLock: %s\n", this);
        }

        /**
         * Because StringBuilder is called so much.
         */
        private class StringBuilderProxy {
            public void append(String s) {
            }

            public String toString() {
                return "";
            }
        }

        private class DebugStringBuilderProxy extends StringBuilderProxy {
            private StringBuilder sb;
            public DebugStringBuilderProxy() {
                sb = new StringBuilder();
            }

            public void append(String s) {
                sb.append(s);
            }

            public String toString() {
                return sb.toString();
            }
        }

        /**
         * This is gross.
         */
        private boolean canWrite(TransactionId tid) {
            boolean rv = false;
            StringBuilderProxy sb;
            if (DEBUG) {
                sb = new DebugStringBuilderProxy();
            }
            else {
                sb = new StringBuilderProxy();
            }

            if (writer != null) {
                sb.append("canWrite: writer exists... ");
                if (!writer.equals(tid)) {
                    sb.append("and is someone else");
                    rv = false;
                }
                else { // writer == tid: reentrant
                    sb.append("and is the same");
                    rv = true;
                }
            }
            else { // writer == null
                sb.append("canWrite: no writer... ");
                if (readers.size() == 0) {
                    sb.append("and no readers");
                    rv = true;
                }
                else if (readers.size() == 1) {
                    sb.append("and one reader... ");
                    if (readers.contains(tid)) {
                        sb.append("which is tid, allowing lock upgrade... ");
                        rv = true;
                    }
                    else {
                        sb.append("not this one");
                        rv = false;
                    }
                } else {
                    sb.append("and many readers");
                    rv = false;
                }
            }

            debug("%s => %b\n", sb.toString(), rv);
            return rv;
        }

        public synchronized void lock(TransactionId tid, Permissions perm) throws TransactionAbortedException{
            debug("lock: tid=%s, perm=%s, this=%s\n", tid, perm, this);
            try {
                if (perm == Permissions.READ_ONLY) {
                    debug("lock: S-lock requested, this=%s\n", this);
                    while (writer != null && !writer.equals(tid)) {
                        debug("lock: other writer exists, sleeping...\n");
                        deadlockGraph.addEdge(tid, writer);

                        if (deadlockGraph.hasDeadlock()) {
                            throw new TransactionAbortedException();
                        }
                        wait();
                    }
                    debug("lock: no writer, adding to readers\n");
                    readers.add(tid);
                }
                else if (perm == Permissions.READ_WRITE) {
                    debug("lock: X-lock requested, this=%s\n", this);
                    while (!canWrite(tid)) {
                        debug("lock: other writer or readers, sleeping...\n");
                        if (writer != null) {
                            deadlockGraph.addEdge(tid, writer);
                        }
                        deadlockGraph.addOutgoingEdges(tid, readers);

                        if (deadlockGraph.hasDeadlock()) {
                            throw new TransactionAbortedException();
                        }
                        wait();
                    }
                    debug("lock: got the okay from canWrite, adding to writers\n");
                    writer = tid;
                    if (writer.equals(tid)) {
                        debug("writer got own lock, increasing count\n");
                    }
                }
                debug("lock: after op; this=%s\n", this);
            }
            catch (InterruptedException e) {
                debug("lock: received InterruptedException=%d\n", e);
                e.printStackTrace();
            }
            debug("lock: reached end of method\n");
        }

        /**
         * Remember kids, release **all** locks.
         * @param tid
         */
        public synchronized void unlock(TransactionId tid) {
            debug("unlock: tid=%s, this=%s\n", tid, this);

            boolean didWork = false;
            if (writer != null && writer.equals(tid)) {
                writer = null;
                didWork = true;
            }
            if (readers.remove(tid)) {
                didWork = true;
            }

            if (!didWork) {
                throw new IllegalMonitorStateException("nothing to unlock");
            }

            deadlockGraph.removeIncomingEdges(tid);
            notifyAll();
        }

        /**
         * @param tid
         * @return if this transaction is also the writer
         */
        public synchronized boolean unlockReader(TransactionId tid) {
            if (!readers.remove(tid)) {
                throw new IllegalMonitorStateException("this transaction never locked");
            }

            deadlockGraph.removeIncomingEdges(tid);
            notifyAll();

            return writer != null && writer.equals(tid);
        }

        public boolean holdsLock(TransactionId tid) {
            return writer.equals(tid) || readers.contains(tid);
        }

        public boolean isLocked() {
            return writer != null || readers.size() != 0;
        }

        public String toString() {
            return String.format("<PageLock readers=%d, writer=%s>",
                    readers.size(), writer);
        }

        public void debug(String format, Object...objects) {
            if (DEBUG) {
                format = String.format("[%tI:%<tM:%<tS.%<tL] %s(%d): %s",
                        new Date(),
                        PageLock.class.getSimpleName(),
                        Thread.currentThread().getId(),
                        format);
                System.out.printf(format, objects);
                System.out.flush();
            }
        }
    }

    /**
     * Waits-for deadlock graph. There is an edge from transaction Ti to Tj if Ti is
     * waiting for Tj to release a lock.
     */

    private class DeadlockGraph {
        private class DeadlockNode {
            public TransactionId tid;
            public LinkedList<TransactionId> outgoing;
            public LinkedList<TransactionId> incoming;

            public DeadlockNode(TransactionId tid) {
                this.tid = tid;
                outgoing = new LinkedList<TransactionId>();
                incoming = new LinkedList<TransactionId>();
            }

            public DeadlockNode(DeadlockNode dn) {
                this.tid = dn.tid;
                outgoing = new LinkedList<TransactionId>(dn.outgoing);
                incoming = new LinkedList<TransactionId>(dn.incoming);
            }
        }
        /**
         * A mapping of TransactionIds to its DeadlockNode
         */
        private ConcurrentHashMap<TransactionId, DeadlockNode> nodes;

        /**
         * The set of transactions waiting on another transaction, but have no
         * transactions waiting on themselves.
         */
        private Set<TransactionId> waitingButNotWaitedOn;

        public DeadlockGraph() {
            nodes = new ConcurrentHashMap<TransactionId, DeadlockNode>();
            waitingButNotWaitedOn = new HashSet<TransactionId>();
        }

        public synchronized void addEdge(TransactionId waiter, TransactionId waitedOn) {
            DeadlockNode waiterNode;
            DeadlockNode waitedOnNode;

            if (nodes.containsKey(waiter)) {
                waiterNode = nodes.get(waiter);
            } else {
                waiterNode = new DeadlockNode(waiter);
            }

            if (!waiterNode.outgoing.contains(waitedOn)) {
                waiterNode.outgoing.add(waitedOn);
            }

            if (nodes.containsKey(waitedOn)) {
                waitedOnNode = nodes.get(waitedOn);
            } else {
                waitedOnNode = new DeadlockNode(waitedOn);
            }

            if (!waitedOnNode.incoming.contains(waiter)) {
                waitedOnNode.incoming.add(waiter);
            }

            if (waitingButNotWaitedOn.contains(waitedOn)) {
                waitingButNotWaitedOn.remove(waitedOn);
            }
        }

        public synchronized void addOutgoingEdges(TransactionId tid, Set<TransactionId> outgoing) {
            for (TransactionId outgoingTid : outgoing) {
                addEdge(tid, outgoingTid);
            }
        }


        public synchronized void removeEdge(TransactionId waiter, TransactionId waitedOn) {
            if (nodes.containsKey(waiter)){
                DeadlockNode waiterNode = nodes.get(waiter);
                waiterNode.outgoing.remove(waitedOn);
            }
            if (nodes.containsKey(waitedOn)){
                DeadlockNode waitedOnNode = nodes.get(waitedOn);
                waitedOnNode.incoming.remove(waiter);

                if (waitedOnNode.incoming.size() == 0) {
                    // No one is waiting on me anymore
                    waitingButNotWaitedOn.add(waitedOnNode.tid);
                }
            }
        }

        public synchronized void removeIncomingEdges(TransactionId waitedOn) {
            if (nodes.containsKey(waitedOn)) {
                DeadlockNode waitedOnNode = nodes.get(waitedOn);
                LinkedList<TransactionId> incoming = new LinkedList<TransactionId>(waitedOnNode.incoming);
                for (TransactionId waiter : incoming) {
                    removeEdge(waiter, waitedOn);
                }
            }
        }


        public synchronized boolean hasDeadlock() {
            HashMap<TransactionId, DeadlockNode> edges = new HashMap<TransactionId, DeadlockNode>();
            LinkedList<TransactionId> waiting = new LinkedList<TransactionId>(waitingButNotWaitedOn);
            LinkedList<TransactionId> sorted = new LinkedList<TransactionId>();
            for (DeadlockNode dn : nodes.values()) {
                edges.put(dn.tid, new DeadlockNode(dn));
            }

            while(!waiting.isEmpty()) {
                TransactionId n = waiting.removeFirst();
                sorted.addLast(n);
                DeadlockNode nNode = edges.get(n);
                for (TransactionId m : nNode.outgoing) {
                    DeadlockNode mNode = edges.get(m);
                    mNode.incoming.remove(n);
                    edges.put(m, mNode);
                    if (mNode.incoming.isEmpty()) {
                        waiting.addLast(m);
                    }
                }
                edges.remove(n);
            }
            if (!edges.isEmpty()) {
                return false;
            } else {
                return true;
            }

        }

    }

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        maxPages = numPages;
        pool = new HashMap<PageId, Page>();
        lruPool = new LinkedList<Page>();
        txns = new ConcurrentHashMap<TransactionId, HashSet<PageId>>();
        lox = new ConcurrentHashMap<PageId, PageLock>();
        deadlockGraph = new DeadlockGraph();
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException {

        // Become aware of this transaction.
        HashSet<PageId> locked = txns.get(tid);
        if (locked == null) {
            locked = new HashSet<PageId>();
        }
        txns.put(tid, locked);

        // Try to get the page lock. If it doesn't exist, we're free to make one.
        PageLock lock = lox.get(pid);
        if (lock == null) {
            lock = new PageLock();
            lox.put(pid, lock);
        }

        lock.lock(tid, perm);


        Page p = pool.get(pid);

        if (p == null) {
            p = getDbForPid(pid).readPage(pid);

            // If no room for the page in the pool, evict a (non-dirty) page
            if (lruPool.size() >= maxPages) {
                evictPage();
            }

            // Now we can add a page into the pool.
            lruPool.add(p);
            pool.put(pid, p);
        } else {
            // Found the page, update LRU
            lruPool.remove(p);
            lruPool.add(p);
        }

        locked.add(pid);
        return p;
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public void releasePage(TransactionId tid, PageId pid) {
        // debug("releasePage: tid=%s, pid=%s\n", tid, pid);
        lox.get(pid).unlock(tid);
        txns.get(tid).remove(pid);
    }

    /**
     * For insertion scanning use only.
     */
    public void releaseReadOnlyPage(TransactionId tid, PageId pid) {
        boolean alsoWriter = lox.get(pid).unlockReader(tid);

        if (!alsoWriter) {
            txns.get(tid).remove(pid);
        }
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId pid) {
        debug("holdsLock: tid=%s, pid=%s\n", tid, pid);
        return lox.get(pid).holdsLock(tid);
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        transactionComplete(tid, true);
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        HashSet<PageId> locked = txns.remove(tid);

        // Some of the tests don't do any writing. If a transaction didn't incur any pages touched, it's in the clear...
        if (locked == null) {
            return;
        }

        debug("transactionComplete: commit=%b, tid=%s => %s locked pages\n", commit, tid, locked);

        // Should have locked null check here
        for (PageId pid : locked) {
            debug("transactionComplete: pid=%s => %s\n", pid, lox.get(pid));
            // Unlock all page locks associated with this transaction
            PageLock lock = lox.get(pid);
            lock.unlock(tid);
            // This doesn't actually work, but oh well.
            if (!lock.isLocked()) {
                lox.remove(pid);
            }
            debug("transactionComplete: after: pid=%s => %s\n", pid, lox.get(pid));

            // Flush all pages or re-read them into the pool, depending on the result of the transaction
            if (commit) {
                flushPage(pid);
            }
            else {
                Page p = getDbForPid(pid).readPage(pid);
                Page old = pool.get(pid);

                // This pid is guaranteed to have been in the pool and LRU.
                // Overwrite the pool.
                pool.put(pid, p);

                // Remove the old version from the LRU...
                lruPool.remove(old);
                // ...and replace with this version
                lruPool.add(p);
            }
        }
        debug("transactionComplete: way after: lox=%s\n", lox);
    }

    /**
     * Add a tuple to the specified table behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to(Lock
     * acquisition is not needed for lab2). May block if the lock cannot
     * be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and updates cached versions of any pages that have
     * been dirtied so that future requests see up-to-date pages.
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        // Page may not exist before the file makes it
        List<Page> touchedPages = Database.getCatalog().getDbFile(tableId).insertTuple(tid, t);

        debug("insertTuple: dirty=%s\n", touchedPages);

        // insertTuple has obtained a lock on these pages **on our behalf**, and
        // told us which page this is.
        for (Page p : touchedPages) {
            p.markDirty(true, tid);
        }
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit.  Does not need to update cached versions of any pages that have
     * been dirtied, as it is not possible that a new page was created during the deletion
     * (note difference from addTuple).
     *
     * Now why on earth this doesn't have the tableId, I don't know.
     *
     * @param tid the transaction adding the tuple.
     * @param t the tuple to add
     */
    public void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, TransactionAbortedException {
        Page page = getPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
        page.markDirty(true, tid);

        getDbForPid(t.getRecordId().getPageId()).deleteTuple(tid, t);
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        for (Map.Entry<PageId, Page> pair : pool.entrySet()) {
            Page pg = (Page) pair.getValue();
            flushPage(pg.getId());
        }
    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
    */
    public synchronized void discardPage(PageId pid) {
        // some code goes here
	// not necessary for proj1
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized void flushPage(PageId pid) throws IOException {
        debug("flushPage: pid=%s\n", pid);
        Page pg = pool.get(pid);
        if (pg.isDirty() != null) {
            DbFile f = getDbForPid(pid);
            f.writePage(pg);
            pg.markDirty(false, null);
        }
    }

    private DbFile getDbForPid(PageId pid) {
        return Database.getCatalog().getDbFile(pid.getTableId());
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized  void flushPages(TransactionId tid) throws IOException {
        debug("flushPages: tid=%s, pages=%s, lru=%s\n", tid, txns.get(tid), lruPool);

        for (PageId pid : txns.get(tid)) {
            flushPage(pid);
        }
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure pages are updated on disk.
     *
     * From spec: In the case where all pages in the buffer pool are dirty, you should throw a DbException.
     */
    private synchronized  void evictPage() throws DbException {
        debug("evictPage: init\n");
        if (lruPool.isEmpty()) {
            throw new DbException("Cannot evict pages from an empty BufferPool");
        }

        int visited = 0;
        Page evictPage;
        // Keep going until we find a page that isn't dirty
        while (true) {
            if (visited == maxPages) {
                throw new DbException("no clean pages to evict");
            }

            evictPage = lruPool.removeFirst();
            if (evictPage.isDirty() == null) {
                // This page is not dirty: we can remove it
                debug("evictPage: removing %s\n", evictPage.getId());
                pool.remove(evictPage.getId());
                return;
            }
            else { // dirty: put it at the back of the list
                lruPool.addLast(evictPage);
                visited++;
            }
        }
    }

    public String toString() {
        return String.format("<BufferPool>");
    }

    public void dump() {
        debug("txns: %s\n\nlox: %s\n", txns, lox);
    }

    public void debug(String format, Object...objects) {
        if (DEBUG) {
            format = String.format("[%tI:%<tM:%<tS.%<tL] %s(%d): %s",
                    new Date(),
                    BufferPool.class.getSimpleName(),
                    Thread.currentThread().getId(),
                    format);
            System.out.printf(format, objects);
            System.out.flush();
        }
    }
}
