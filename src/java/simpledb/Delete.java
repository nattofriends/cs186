package simpledb;

import java.io.IOException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;

    private static final TupleDesc SCHEMA = new TupleDesc(new Type[] { Type.INT_TYPE } );

    private TransactionId tid;
    private DbIterator child;

    private boolean seen = false;

    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     *
     * @param tid
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
    public Delete(TransactionId tid, DbIterator child) {
        this.tid = tid;
        this.child = child;
    }

    public TupleDesc getTupleDesc() {
        return SCHEMA;
    }

    public void open() throws DbException, TransactionAbortedException {
        child.open();
        super.open();
    }

    public void close() {
        child.close();
        super.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        child.rewind();
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     *
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (seen) {
            return null;
        }

        int count = 0;
        while (child.hasNext()) {
            Database.getBufferPool().deleteTuple(tid, child.next());
            count++;
        }

        Tuple rv = new Tuple(SCHEMA);
        rv.setField(0, new IntField(count));

        seen = true;
        return rv;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[] { child };
    }


    @Override
    public void setChildren(DbIterator[] children) {
        // This should throw up instead.
        if (children.length != 1) {
            return;
        }

        child = children[0];
    }

    public String toString() {
        return String.format("<Delete child=%s>", child);
    }

}
