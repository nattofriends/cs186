package simpledb;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator {

    private static final long serialVersionUID = 1L;

    private static final TupleDesc SCHEMA = new TupleDesc(new Type[] { Type.INT_TYPE } );

    private TransactionId tid;
    private DbIterator child;
    private int tableId;

    private boolean seen = false;

    /**
     * Constructor.
     *
     * @param tid
     *            The transaction running the insert.
     * @param child
     *            The child operator from which to read tuples to be inserted.
     * @param tableId
     *            The table in which to insert tuples.
     * @throws DbException
     *             if TupleDesc of child differs from table into which we are to
     *             insert.
     */
    public Insert(TransactionId tid, DbIterator child, int tableId)
            throws DbException {
        this.tid = tid;
        this.child = child;
        this.tableId = tableId;
    }

    public TupleDesc getTupleDesc() {
        return SCHEMA;
    }

    public void open() throws DbException, TransactionAbortedException {
        child.open();
        super.open();
    }

    public void close() {
        child.close();
        super.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        child.rewind();
    }

    /**
     * Inserts tuples read from child into the tableid specified by the
     * constructor. It returns a one field tuple containing the number of
     * inserted records. Inserts should be passed through BufferPool. An
     * instances of BufferPool is available via Database.getBufferPool(). Note
     * that insert DOES NOT need check to see if a particular tuple is a
     * duplicate before inserting it.
     *
     * @return A 1-field tuple containing the number of inserted records, or
     *         null if called more than once.
     * @see Database#getBufferPool
     * @see BufferPool#insertTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (seen) {
            return null;
        }

        int count = 0;
        while (child.hasNext()) {
            try {
                Database.getBufferPool().insertTuple(tid, tableId, child.next());
            } catch (IOException e) { // because insertTuple is declared to throw IOException... ~_~
                e.printStackTrace();
            }
            count++;
        }

        Tuple rv = new Tuple(SCHEMA);
        rv.setField(0, new IntField(count));

        seen = true;
        return rv;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[] { child };
    }


    @Override
    public void setChildren(DbIterator[] children) {
        // This should throw up instead.
        if (children.length != 1) {
            return;
        }

        child = children[0];
    }

    public String toString() {
        return String.format("<Insert tableId=%d, child=%s>", tableId, child);
    }

}
