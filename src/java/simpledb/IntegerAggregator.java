package simpledb;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    protected class OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            throw new UnsupportedOperationException();
        }
    }

    protected class AssignmentHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = nextValue;
            return data;
        }
    }

    protected class DoNothingHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            return data;
        }
    }

    protected class ZeroHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = new BigDecimal(0);
            return data;
        }
    }

    protected class InitAverageHandler extends OperationHandler {
        public InitAverageHandler() {}
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = new BigDecimal(0);
            data.opaque.add(0);
            return data;
        }
    }

    protected class MinHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = data.value.min(nextValue);
            return data;
        }
    }

    protected class MaxHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = data.value.max(nextValue);
            return data;
        }
    }

    protected class SumHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = data.value.add(nextValue);
            return data;
        }
    }

    protected class CountHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            data.value = data.value.add(BigDecimal.ONE);
            return data;
        }
    }

    protected class AverageHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            // data.value is the sum, data.opaque[0] is the number of items.
            data.value = data.value.add(nextValue);
            data.opaque.set(0, data.opaque.get(0) + 1);
            return data;
        }
    }

    protected class FinishAverageHandler extends OperationHandler {
        public AggregateData process(AggregateData data, BigDecimal nextValue) {
            // The test case calls the iterator through each step of averaging. So don't write the results back!
            AggregateData rv = new AggregateData();
            rv.value = data.value.divide(new BigDecimal(data.opaque.get(0)), MathContext.DECIMAL128);
            return rv;
        }
    }

    private class Operations {
        public OperationHandler pre;
        public OperationHandler during;
        public OperationHandler post;

        public Operations(Class<? extends OperationHandler> pre,
                Class<? extends OperationHandler> during, Class<? extends OperationHandler> post) {
            try {
                this.pre = pre.getDeclaredConstructor(IntegerAggregator.class)
                        .newInstance(IntegerAggregator.this);
                this.during = during.getDeclaredConstructor(IntegerAggregator.class)
                        .newInstance(IntegerAggregator.this);
                this.post = post.getDeclaredConstructor(IntegerAggregator.class)
                        .newInstance(IntegerAggregator.this);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            }

        }
    }

    private class IntegerAggregateIterator implements DbIterator {
        private static final long serialVersionUID = 1L;

        private boolean groupless;
        private Iterable<Entry<Field, AggregateData>> iterable;
        private Iterator<Entry<Field, AggregateData>> it;
        private TupleDesc schema;

        public IntegerAggregateIterator(boolean groupless, Iterable<Entry<Field, AggregateData>> iterable, TupleDesc schema) {
            this.groupless = groupless;
            this.iterable = iterable;
            this.it = null;
            this.schema = schema;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            it = iterable.iterator();
        }

        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if (it == null) {
                return false;
            }

            return it.hasNext();
        }

        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if (it == null) {
                throw new NoSuchElementException();
            }

            Entry<Field, AggregateData> e = it.next();

            Tuple t = new Tuple(schema);

            AggregateData data = e.getValue();
            data = IntegerAggregator.this.opHandlers.post.process(data, null);

            if (groupless) {
                t.setField(0, new IntField(data.value.intValue()));
            } else {
                t.setField(0, e.getKey());
                t.setField(1, new IntField(data.value.intValue()));
            }

            return t;
        }

        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            if (it != null) {
                it = iterable.iterator();
            } else {
                throw new DbException("iterator not open");
            }
        }

        /**
         * Can't throw anything here, so let people close it over and over.
         *
         */
        @Override
        public void close() {
            it = null;
        }

        @Override
        public TupleDesc getTupleDesc() {
            return schema;
        }
    }

    // Thanks for this, AVG.
    private class AggregateData {
        // Arbitrary-precision because of AVG.
        public BigDecimal value;
        // Need to keep count of items seen because of AVG.
        public ArrayList<Integer> opaque;

        public AggregateData() {
            opaque = new ArrayList<Integer>();
        }

        public String toString() {
            return String.format("<AggregateData value=%s, opaque=%s>", value, opaque);
        }
    }

    private static final long serialVersionUID = 1L;

    private int gbField, aggField;
    private Type gbFieldType;
    private Op op;
    private Tuple result;
    private TupleDesc resultSchema;
    private Operations opHandlers;

    // Note: null key for no grouping
    private Map<Field, AggregateData> aggregate;

    /**
     * Aggregate constructor
     *
     * @param gbField
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbFieldType
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param aggField
     *            the 0-based index of the aggregate field in the tuple
     * @param op
     *            the aggregation operator
     */

    public IntegerAggregator(int gbField, Type gbFieldType, int aggField, Op op) {
        this.gbField = gbField;
        this.gbFieldType = gbFieldType;
        this.aggField = aggField;
        this.op = op;
        this.aggregate = new HashMap<Field, AggregateData>();

        if (this.gbField == Aggregator.NO_GROUPING) {
            this.resultSchema = new TupleDesc(new Type[] { Type.INT_TYPE });
        } else {
            this.resultSchema = new TupleDesc(new Type[] { gbFieldType, Type.INT_TYPE });
        }

        switch (op) {
        case MIN:
            this.opHandlers = new Operations(AssignmentHandler.class, MinHandler.class, DoNothingHandler.class);
            break;
        case MAX:
            this.opHandlers = new Operations(AssignmentHandler.class, MaxHandler.class, DoNothingHandler.class);
            break;
        case SUM:
            this.opHandlers = new Operations(ZeroHandler.class, SumHandler.class, DoNothingHandler.class);
            break;
        case AVG:
            this.opHandlers = new Operations(InitAverageHandler.class, AverageHandler.class, FinishAverageHandler.class);
            break;
        case COUNT:
            this.opHandlers = new Operations(ZeroHandler.class, CountHandler.class, DoNothingHandler.class);
            break;
        }

    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     *
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // NO_GROUPING means everything goes in the null bucket
        Field gb = gbField == Aggregator.NO_GROUPING ? null : tup.getField(gbField);

        // This is IntField otherwise this aggregator wouldn'tve been chosen.
        BigDecimal aggValue = new BigDecimal(((IntField) tup.getField(aggField)).getValue());
        AggregateData data = aggregate.get(gb);
        if (data == null) {
            data = new AggregateData();

            data = this.opHandlers.pre.process(data, aggValue);
            aggregate.put(gb, data);
        }

        data = this.opHandlers.during.process(data, aggValue);
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        return new IntegerAggregateIterator(gbField == Aggregator.NO_GROUPING, aggregate.entrySet(), resultSchema);
    }

    public String toString() {
        return String.format("<IntegerAggregator gbField=%d, gbFieldType=%s, aggField=%d, op=%s>", gbField, gbFieldType, aggField, op);
    }

}
