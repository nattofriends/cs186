package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 *
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    private static final long serialVersionUID = 1L;
    private File f;
    private RandomAccessFile file;
    private TupleDesc td;

    private class HeapFileIterator implements DbFileIterator {
        private static final boolean DEBUG = false;
        private static final long serialVersionUID = 1L;
        private int numPages;
        private int currentPage = 0;
        private Iterator<Tuple> pageIterator;
        private TransactionId tid;

        public HeapFileIterator(int numPages, TransactionId tid) {
            this.numPages = numPages;
            this.tid = tid;
            debug("%s\n", this);
        }

        private Iterator<Tuple> getIteratorForPage(int pageNo) throws TransactionAbortedException, DbException {
            return HeapFile.this.getBufferedPage(tid, pageNo, Permissions.READ_ONLY).iterator();
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            debug("open: init\n");
            if (pageIterator == null) {
                debug("open: no pageIterator, getting page 0\n");
                pageIterator = getIteratorForPage(0);
                currentPage = 0;
            } else {
                throw new DbException("iterator already open");
            }
        }

        /**
         * See @94 for some behavior guidelines
         */
        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if (pageIterator == null) {
                return false;
            }

            if (pageIterator.hasNext()) {
                return true;
            }

            // Are we at the last page?
            if (currentPage == numPages - 1) {
                debug("hasNext: at last page, returning false\n");
                return false;
            }

            // Go to the next page.
            currentPage++;
            pageIterator = getIteratorForPage(currentPage);
            // Next page not guaranteed to have it -- be lazy and recurse to check it...
            // Didn't we explicitly loop for this kind of thing somewhere else? Oh well.
            return hasNext();
        }

        /**
         * See @94 for some behavior guidelines
         */
        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if (pageIterator == null) {
                throw new NoSuchElementException();
            }
            return pageIterator.next();
        }

        /**
         * Like {@link #open()}, but the iterator should have been open already.
         * <p>
         * According to @94, can be implemented as {@link #close()} then {@link #open()},
         * but @147 says that's not necessarily the right thing to do.
         */
        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            debug("rewind: init\n");
            if (pageIterator != null) {
                debug("rewind: we have a pageIterator, setting it to page 0\n");
                pageIterator = getIteratorForPage(0);
                currentPage = 0;
            } else {
                throw new DbException("iterator not open");
            }
        }

        /**
         * Can't throw anything here, so let people close it over and over.
         *
         */
        @Override
        public void close() {
            debug("close: closing pageIterator\n");
            pageIterator = null;
        }

        public String toString() {
            return String.format("<HeapFileIterator of %s>", HeapFile.this);
        }

        public void debug(String format, Object...objects) {
            if (DEBUG)
                System.out.printf(format, objects);
        }
    }

    /**
     * Constructs a heap file backed by the specified file.
     *
     * @param f the file that stores the on-disk backing store for this heap file.
     */
    public HeapFile(File f, TupleDesc td) {
        this.f = f;
        try {
            this.file = new RandomAccessFile(f, "rw");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.td = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     *
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        return f;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     *
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        return f.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     *
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        return td;
    }

    /**
     * Because of @77, we can assume that <code>pid</code> is of type {@link HeapPageId}.
     */
    public Page readPage(PageId pid) {
        HeapPageId hpid = (HeapPageId) pid;
        byte[] buf = new byte[BufferPool.PAGE_SIZE];

        try {
            // Do actual seeking in the file here.
            file.seek(BufferPool.PAGE_SIZE * hpid.pageNumber());
            // This is the offset into the byte[], not the file.
            file.readFully(buf, 0, BufferPool.PAGE_SIZE);
            return (Page) new HeapPage(hpid, buf);
        } catch (IOException e) {
             e.printStackTrace();
        }
        return null;
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        try {
            file.seek(BufferPool.PAGE_SIZE * page.getId().pageNumber());
            file.write(page.getPageData(), 0, BufferPool.PAGE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        try {
            return (int) Math.ceil((double) file.length() / BufferPool.PAGE_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        ArrayList<Page> rv = new ArrayList<Page>();
        HeapPage page = null;

        // From project spec:
        // Looking for an empty slot into which you can insert tuples.
        // Most implementations scan pages looking for an empty slot, and will need a READ_ONLY lock to do this.
        // Surprisingly, however, if a transaction t finds no free slot on a page p, t may immediately
        // release the lock on p. Although this apparently contradicts the rules of two-phase locking,
        // it is ok because t did not use any data from the page, such that a concurrent
        // transaction t' which updated p cannot possibly effect the answer or outcome of t.

        // Scan with READ_ONLY locks up until the point where we find a page to insert on.
        // This means that all the pages up until i will be ro-locked for the duration
        // of the insert, but it's better than being rw-locked.
        // Use hax to release the page lock on scans, and then upgrade the lock on the page we care about.

        boolean found = false;
        int i;
        for (i = 0; i < numPages(); i++) {
            HeapPage tryPage = getBufferedPage(tid, i, Permissions.READ_ONLY);
            if (tryPage.getNumEmptySlots() > 0) {
                found = true;
                break;
            }
            Database.getBufferPool().releaseReadOnlyPage(tid, new HeapPageId(this.getId(), i));
        }
        if (found) {
            page = getBufferedPage(tid, i, Permissions.READ_WRITE);
        }

        if (page == null) {
            file.seek(file.length());
            file.write(HeapPage.createEmptyPageData());

            // numPages() - 1 because we just wrote a new page
            page = getBufferedPage(tid, numPages() - 1, Permissions.READ_WRITE);
        }

        rv.add(page);
        page.insertTuple(t);
        return rv;
    }

    /**
     * Note: this is not tested in the provided unit tests
     */
    public Page deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        HeapPage page = getBufferedPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
        page.deleteTuple(t);
        return page;
    }

    private HeapPage getBufferedPage(TransactionId tid, int pageNo, Permissions perm) throws TransactionAbortedException, DbException {
        return getBufferedPage(tid, new HeapPageId(this.getId(), pageNo), perm);
    }

    private HeapPage getBufferedPage(TransactionId tid, PageId pageId, Permissions perm) throws TransactionAbortedException, DbException {
        return (HeapPage) Database.getBufferPool()
                .getPage(tid, pageId, perm);
    }

    public DbFileIterator iterator(TransactionId tid) {
        return new HeapFileIterator(numPages(), tid);
    }

    public String toString() {
        return String.format("<HeapFile file=%s, schema=%s>", f, td);
    }

}

