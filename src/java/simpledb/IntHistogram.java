package simpledb;

import java.util.Arrays;

import simpledb.Predicate.Op;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {
    private static final boolean DEBUG = false;
    // natto bucket selection uses double bucket widths and uses the entire array but has gaps in between, and
    // uses effectiveWidth in calculations = Math.max(1.0, intervalWidth).
    // carriercat bucket selection uses integer bucket widths and only uses the first (max - min) buckets,
    // and leaves the rest empty.
    private static final boolean USE_NATTO_BUCKET_SELECTION = true;

    private int[] histogram;
    private double intervalWidth;
    private int min, max, total;

    /**
     * Create a new IntHistogram.
     *
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     *
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     *
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't
     * simply store every value that you see in a sorted list.
     *
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
        this.histogram = new int[buckets];
        this.total = 0;
        this.min = min;
        this.max = max;
        this.intervalWidth = (double) (max - min + 1) / buckets;
    }

    public void addValue(int v) {
        int bucket;
        // @743:
        // If the bucket width is calculated to be less than 1, we round it up to 1.
        double effectiveWidth = Math.max(1.0, intervalWidth);

        if (USE_NATTO_BUCKET_SELECTION) {
            bucket = (int) ((v - min) / intervalWidth);

            debug("addValue: adding value %d to bucket %d\n", v, bucket);
            histogram[bucket]++;
            total++;
        }

        else {
            for (int i = 0; i < histogram.length; i++) {
                int currentMin = (int) (min + effectiveWidth * i);
                int currentMax = (int) (min + (effectiveWidth * (i + 1)) - 1);
                if (currentMax < max) {
                    if (currentMin <= v && v <= currentMax) {
                        histogram[i] = histogram[i] + 1;
                        total++;
                        return;
                    }
                } else {
                    return;
                }
            }
            throw new Error("Something is wrong");
        }
    }

    public double estimateSelectivity(Predicate.Op op, int v) {
        debug("IntHistogram#estimateSelectivity: op=%s, v=%d\n", op, v);

        // @743:
        // If the bucket width is calculated to be less than 1, we round it up to 1.
        double effectiveWidth = Math.max(1.0, intervalWidth);

        // Bail quickly if something silly is requested
        if (v < min) {
            debug("estimateSelectivity: v < min..\n");
            // There are no values that are less than the minimum value.
            if (op == Op.LESS_THAN || op == Op.LESS_THAN_OR_EQ || op == Op.EQUALS) {
                debug("estimateSelectivity: op was one of <, <=, or =, returning 0.0\n");
                return 0.0;
            }
            else if (op == Op.GREATER_THAN || op == Op.GREATER_THAN_OR_EQ || op == Op.NOT_EQUALS) {
                debug("estimateSelectivity: op was one of >, >=, !=, returning 1.0\n");
                return 1.0;
            }
        } else if (v > max) {
            debug("estimateSelectivity: v > max...\n");
            if (op == Op.GREATER_THAN || op == Op.GREATER_THAN_OR_EQ || op == Op.EQUALS) {
                debug("estimateSelectivity: op was one of >, >=, or =, returning 0.0\n");
                return 0.0;
            }
            else if (op == Op.LESS_THAN || op == Op.LESS_THAN_OR_EQ || op == Op.NOT_EQUALS) {
                debug("estimateSelectivity: op was one of <, <=, !=, returning 1.0\n");
                return 1.0;
            }
        }

        int bucket = 0;
        if (USE_NATTO_BUCKET_SELECTION) {
            bucket = (int) ((v - min) / intervalWidth);
        } else {
            bucket = -1;

            for (int i = 0; i < histogram.length; i++) {
                int currentMin = (int) (min + effectiveWidth * i);
                int currentMax = (int) (min + (effectiveWidth * (i + 1)) - 1);
                if (currentMin <= max) {
                    if (currentMin <= v && v <= currentMax) {
                        bucket = i;
                        break;
                    }
                } else {
                    break;
                }
            }
        }

        debug("%s\n", this);
        debug("estimateSelectivity: bucket=%d, h[b]=%d, total=%d, histogram=%s\n",
                bucket,
                histogram[bucket],
                total,
                Arrays.toString(histogram));

        double equalitySelectivity;

        switch (op) {
        case EQUALS:
            equalitySelectivity = ((double) histogram[bucket] / effectiveWidth) / total;
            debug("equalitySelectivity: returning %f\n", equalitySelectivity);
            return equalitySelectivity;
        case NOT_EQUALS:
            equalitySelectivity = ((double) histogram[bucket] / effectiveWidth) / total;
            debug("equalitySelectivity: returning %f\n", 1.0 - equalitySelectivity);
            return 1.0 - equalitySelectivity;
        case GREATER_THAN:
            return getInequalitySelectivity(v, bucket, true, true);
        case GREATER_THAN_OR_EQ:
            return getInequalitySelectivity(v, bucket, true, false);
        case LESS_THAN:
            return getInequalitySelectivity(v, bucket, false, true);
        case LESS_THAN_OR_EQ:
            return getInequalitySelectivity(v, bucket, false, false);
        default:
            break;
        }

        return -1.0;
    }

    /**
     * @param v
     * @param bucket
     * @param isGreaterThan
     * @param exclusive
     * @return
     */
    private double getInequalitySelectivity(int v, int bucket, boolean isGreaterThan, boolean exclusive) {
        int skipV = exclusive ? 0 : 1;
        skipV = isGreaterThan ? -skipV : skipV;

        double effectiveWidth = Math.max(1.0, intervalWidth);

        double bucketEndpoint = min + (intervalWidth * bucket) + (isGreaterThan ? (effectiveWidth - 1) : 0);
        double partialWidth = Math.abs(bucketEndpoint - v - skipV);


        debug("getInequalitySelectivity: v=%d, bucket=%d, isGreaterThan=%b, exclusive=%b, skipV=%d, "
                + "bucketEndpoint=%f, partialWidth=%f\n",
                v, bucket, isGreaterThan, exclusive, skipV, bucketEndpoint, partialWidth);

        double selectivity;
        if (partialWidth == 0.0) {
            selectivity = 0.0;
        } else{
            selectivity = ((double) histogram[bucket] / partialWidth) / total;
        }

        debug("getInequalitySelectivity: selectivity=%f for bucket %d\n", selectivity, bucket);

        if (isGreaterThan) {
            for (int i = bucket + 1; i < histogram.length; i++) {
                selectivity += ((double) histogram[i] / effectiveWidth) / total;
                debug("getInequalitySelectivity: adding bucket selectivity %f for bucket %d\n",
                        ((double) histogram[i] / effectiveWidth) / total, i);
            }
        } else {
            for (int i = 0; i < bucket; i++) {
                selectivity += ((double) histogram[i] / effectiveWidth) / total;
                debug("getInequalitySelectivity: adding bucket selectivity %f for bucket %d\n",
                        ((double) histogram[i] / effectiveWidth) / total, i);
            }
        }

        debug("getInequalitySelectivity: after summing, selectivity=%f\n", selectivity);

        return selectivity;
    }

    public double avgSelectivity()
    {
        // some code goes here
        return 1.0;
    }

    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        return String.format("<IntHistogram min=%d, max=%d, intervalWidth=%f, buckets=%d, total=%d>",
                min, max, intervalWidth, histogram.length, total);
    }

    private void debug(String format, Object...objects) {
        if (DEBUG)
            System.out.printf(format, objects);
    }
}
