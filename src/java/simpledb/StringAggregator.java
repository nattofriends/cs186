package simpledb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Map.Entry;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private class StringAggregateIterator implements DbIterator {
        private static final long serialVersionUID = 1L;

        private boolean groupless;
        private Iterable<Entry<Field, Integer>> iterable;
        private Iterator<Entry<Field, Integer>> it;
        private TupleDesc schema;

        public StringAggregateIterator(boolean groupless, Iterable<Entry<Field, Integer>> iterable, TupleDesc schema) {
            this.groupless = groupless;
            this.iterable = iterable;
            this.it = null;
            this.schema = schema;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            it = iterable.iterator();
        }

        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if (it == null) {
                return false;
            }

            return it.hasNext();
        }

        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if (it == null) {
                throw new NoSuchElementException();
            }

            Entry<Field, Integer> e = it.next();

            Tuple t = new Tuple(schema);

            if (groupless) {
                t.setField(0, new IntField(e.getValue()));
            } else {
                t.setField(0, e.getKey());
                t.setField(1, new IntField(e.getValue()));
            }

            return t;
        }

        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            if (it != null) {
                it = iterable.iterator();
            } else {
                throw new DbException("iterator not open");
            }
        }

        /**
         * Can't throw anything here, so let people close it over and over.
         *
         */
        @Override
        public void close() {
            it = null;
        }

        @Override
        public TupleDesc getTupleDesc() {
            return schema;
        }
    }

    private static final long serialVersionUID = 1L;

    private int gbField, aggField;
    private Type gbFieldType;
    private Op op;
    private TupleDesc resultSchema;

    // Note: null key for no grouping
    // Slightly simpler one for StringAggregator
    private HashMap<Field, Integer> aggregate;

    /**
     * Aggregate constructor
     * @param gbField the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbFieldType the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param aField the 0-based index of the aggregate field in the tuple
     * @param op aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbField, Type gbFieldType, int aggField, Op op) throws IllegalArgumentException {
        if (op != Aggregator.Op.COUNT) {
            throw new IllegalArgumentException("only COUNT is supported");
        }

        this.gbField = gbField;
        this.gbFieldType = gbFieldType;
        this.aggField = aggField;
        this.op = op;
        this.aggregate = new HashMap<Field, Integer>();

        if (this.gbField == Aggregator.NO_GROUPING) {
            this.resultSchema = new TupleDesc(new Type[] { Type.INT_TYPE });
        } else {
            this.resultSchema = new TupleDesc(new Type[] { gbFieldType, Type.INT_TYPE });
        }
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // NO_GROUPING means everything goes in the null bucket
        Field gb = gbField == Aggregator.NO_GROUPING ? null : tup.getField(gbField);

        // We're just counting here, folks
        Integer data = aggregate.get(gb);
        if (data == null) {
            data = new Integer(0);
            aggregate.put(gb, data);
        }

        data += 1;
        aggregate.put(gb, data);
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
        return new StringAggregateIterator(gbField == Aggregator.NO_GROUPING, aggregate.entrySet(), resultSchema);
    }

    public String toString() {
        return String.format("<StringAggregator gbField=%d, gbFieldType=%s, aggField=%d, op=%s>", gbField, gbFieldType, aggField, op);
    }

}
