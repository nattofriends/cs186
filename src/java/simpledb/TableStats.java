package simpledb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TableStats represents statistics (e.g., histograms) about base tables in a
 * query.
 *
 * This class is not needed in implementing proj1 and proj2.
 */
public class TableStats {
    private static final boolean DEBUG = false;
    private static final boolean USE_ITERATOR_WORKAROUND = false;

    private static final ConcurrentHashMap<String, TableStats> statsMap = new ConcurrentHashMap<String, TableStats>();

    static final int IOCOSTPERPAGE = 1000;

    public static TableStats getTableStats(String tablename) {
        return statsMap.get(tablename);
    }

    public static void setTableStats(String tablename, TableStats stats) {
        statsMap.put(tablename, stats);
    }

    public static void setStatsMap(HashMap<String,TableStats> s)
    {
        try {
            java.lang.reflect.Field statsMapF = TableStats.class.getDeclaredField("statsMap");
            statsMapF.setAccessible(true);
            statsMapF.set(null, s);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static Map<String, TableStats> getStatsMap() {
        return statsMap;
    }

    public static void computeStatistics() {
        Iterator<Integer> tableIt = Database.getCatalog().tableIdIterator();

        System.out.println("Computing table stats.");
        while (tableIt.hasNext()) {
            int tableid = tableIt.next();
            TableStats s = new TableStats(tableid, IOCOSTPERPAGE);
            setTableStats(Database.getCatalog().getTableName(tableid), s);
        }
        System.out.println("Done.");
    }

    /**
     * Number of bins for the histogram. Feel free to increase this value over
     * 100, though our tests assume that you have at least 100 bins in your
     * histograms.
     */
    static final int NUM_HIST_BINS = 100;

    // Use ordering from schema
    private ArrayList<Object> histograms;
    private int ioCostPerPage, tableId, total;
    private TupleDesc schema;

    /**
     * Create a new TableStats object, that keeps track of statistics on each
     * column of a table
     *
     * @param tableId
     *            The table over which to compute statistics
     * @param ioCostPerPage
     *            The cost per page of IO. This doesn't differentiate between
     *            sequential-scan IO and disk seeks.
     */
    public TableStats(int tableId, int ioCostPerPage) {
        this.tableId = tableId;
        this.ioCostPerPage = ioCostPerPage;

        histograms = new ArrayList<Object>();

        debug("%s\n", this);

        DbFile table = Database.getCatalog().getDbFile(tableId);
        schema = table.getTupleDesc();

        debug("init: schema has %d fields\n", schema.numFields());

        // Should probably not do full table scans x #fields

        // Iterate over all fields and create TableStats for them
        // Despite the interface we have declared, items is ArrayList<TDItem>.
        // Let us depend on it being this way.

        // First, get the min and max vals of each table...
        TransactionId tid = new TransactionId();
        DbFileIterator it = table.iterator(tid);
        // Will not be all filled
        int[] mins = new int[schema.numFields()];
        int[] maxes = new int[schema.numFields()];

        Arrays.fill(mins, Integer.MAX_VALUE);
        Arrays.fill(maxes, Integer.MIN_VALUE);

        // Which fields?
        ArrayList<Integer> intFields = new ArrayList<Integer>();
        for (int i = 0; i < schema.numFields(); i++) {
            if (schema.getFieldType(i) == Type.INT_TYPE) {
                intFields.add(i);
            }
        }

        debug("init: intFields=%s\n", intFields);

        Tuple tup;
        int fieldVal;
        try {
            // Why must this be so annoying
            // Get the minimum and maximum for IntHistogram initialization
            debug("init: getting extremes for hist init\n");
            it.open();
            while (it.hasNext()) {
                tup = it.next();
                total++;
                for (int i : intFields) {
                    fieldVal = ((IntField) tup.getField(i)).getValue();
                    // debug("value of field %d is %d\n", i, fieldVal);
                    mins[i] = Math.min(mins[i], fieldVal);
                    maxes[i] = Math.max(maxes[i], fieldVal);
                }
            }

            debug("init: got %d tuples\n", total);
            // Now we can create the histograms
            debug("init: creating histograms\n");
            Object hist = null;
            for (int i = 0; i < schema.numFields(); i++) {
                Type t = schema.getFieldType(i);
                debug("init: type of field %d is %s\n", i, t);
                switch (t) {
                case INT_TYPE:
                    debug("init: creating IntHistogram\n");
                    hist = new IntHistogram(NUM_HIST_BINS, mins[i], maxes[i]);
                    debug("hist=%s\n", hist);
                    break;
                case STRING_TYPE:
                    debug("init: creating StringHistogram\n");
                    hist = new StringHistogram(NUM_HIST_BINS);
                    break;
                }
                histograms.add(i, hist);
            }

            // Final table scan for actually adding the values
            String fieldStringVal = null;

            if (USE_ITERATOR_WORKAROUND) {
                debug("TableStats#init: closing and opening iterator...\n");
                // Don't do this, this is bad.
                it.close();
                it = table.iterator(tid);
                it.open();
            } else {
                debug("TableStats#init: rewinding iterator...\n");
                it.rewind();
            }

            HeapFile hf = (HeapFile) Database.getCatalog().getDbFile(tableId);
            int insertionTotal = 0;
            debug("num pages=%d\n", hf.numPages());
            while (it.hasNext()) {
                tup = it.next();
                insertionTotal++;
                for (int i = 0; i < schema.numFields(); i++) {
                    Type t = schema.getFieldType(i);
                    hist = histograms.get(i);
                    switch (t) {
                    case INT_TYPE:
                        fieldVal = ((IntField) tup.getField(i)).getValue();
                        ((IntHistogram) hist).addValue(fieldVal);
                        break;
                    case STRING_TYPE:
                        fieldStringVal = ((StringField) tup.getField(i)).getValue();
                        ((StringHistogram) hist).addValue(fieldStringVal);
                        break;
                    }
                }
            }
            debug("init: finished populating histograms with %d tuples\n", insertionTotal);

        } catch (DbException e) {
            debug("ERROR: caught DbException\n");
            e.printStackTrace();
        } catch (TransactionAbortedException e) {
            debug("ERROR: caught TransactionAbortedException\n");
            e.printStackTrace();
        }

        it.close();
    }

    /**
     * Estimates the cost of sequentially scanning the file, given that the cost
     * to read a page is costPerPageIO. You can assume that there are no seeks
     * and that no pages are in the buffer pool.
     *
     * Also, assume that your hard drive can only read entire pages at once, so
     * if the last page of the table only has one tuple on it, it's just as
     * expensive to read as a full page. (Most real hard drives can't
     * efficiently address regions smaller than a page at a time.)
     *
     * @return The estimated cost of scanning the table.
     */
    public double estimateScanCost() {
        // @795
        HeapFile hf = (HeapFile) Database.getCatalog().getDbFile(tableId);
        return hf.numPages() * ioCostPerPage;
    }

    /**
     * This method returns the number of tuples in the relation, given that a
     * predicate with selectivity selectivityFactor is applied.
     *
     * @param selectivityFactor
     *            The selectivity of any predicates over the table
     * @return The estimated cardinality of the scan with the specified
     *         selectivityFactor
     */
    public int estimateTableCardinality(double selectivityFactor) {
        return (int) (total * selectivityFactor);
    }

    /**
     * The average selectivity of the field under op.
     * @param field
     *        the index of the field
     * @param op
     *        the operator in the predicate
     * The semantic of the method is that, given the table, and then given a
     * tuple, of which we do not know the value of the field, return the
     * expected selectivity. You may estimate this value from the histograms.
     * */
    public double avgSelectivity(int field, Predicate.Op op) {
        // some code goes here
        return 1.0;
    }

    /**
     * Estimate the selectivity of predicate <tt>field op constant</tt> on the
     * table.
     *
     * <pre>On passing TableStatsTest: @839: "Noted. Will increase this margin to a large enough value that accepts all close-enough solutions."
     *
     * @param field
     *            The field over which the predicate ranges
     * @param op
     *            The logical operation in the predicate
     * @param constant
     *            The value against which the field is compared
     * @return The estimated selectivity (fraction of tuples that satisfy) the
     *         predicate
     */
    public double estimateSelectivity(int field, Predicate.Op op, Field constant) {
        debug("TableStats#estimateSelectivity: field=%d, op=%s, value=%s\n", field, op, constant);
        Object hist = histograms.get(field);

        switch (schema.getFieldType(field)) {
        case INT_TYPE:
            IntHistogram intHist = (IntHistogram) hist;
            return intHist.estimateSelectivity(op, ((IntField) constant).getValue());
        case STRING_TYPE:
            StringHistogram strHist = (StringHistogram) hist;
            return strHist.estimateSelectivity(op, ((StringField) constant).getValue());
        default:
            // This shouldn't happen!
            return 0.0;
        }
    }

    /**
     * return the total number of tuples in this table
     * */
    public int totalTuples() {
        return total;
    }

    public String toString() {
        return String.format("<TableStats tableId=%d, ioCostPerPage=%d>", tableId, ioCostPerPage);
    }

    public void debug(String format, Object...objects) {
        if (DEBUG)
            System.out.printf(format, objects);
    }

}
